using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Scrutor;
using TemplateProject.Query;

namespace TemplateProject.ServiceCollectionExtensions
{
    public static class ServiceCollectionExtensions
    {
        static Type[] Types = new[]
        {
            typeof(IQuery<,>),
            typeof(IAsyncQuery<,>),
            typeof(IHandler<>),
            typeof(IAsyncHandler<>),
            typeof(IAsyncQueryHandler<,>),
            typeof(IQueryHandler<>)
        };

        private static IEnumerable<Assembly> allAssembless { get; } = AppDomain.CurrentDomain.GetAssemblies();

        public static IServiceCollection AddAutoRegistration(this IServiceCollection sc)
        {
            sc.Scan(x => x.FromAssemblies(allAssembless)
                .AddClasses(c => c.AssignableToAny(Types)).AsImplementedInterfaces()
                .WithScopedLifetime());
            return sc;
        }
    }
}