using System.Threading.Tasks;
using TemplateProject.Query;

namespace TemplateProject.Controllers.Test
{
    public class TestQuery : IQuery<int, int>
    {
        public int Query(int input)
        {
            return 1;
        }
    }

    public class TestAsyncQuery : IAsyncQuery<int, int>
    {
        public Task<int> Query(int input)
        {
            return Task.FromResult(1);
        }
    }

    public class TestHandler : IHandler<int>
    {
        public void Handle(int input)
        {
            return;
        }
    }

    public class TaskAsyncHandler : IAsyncHandler<int>
    {
        public async Task Handle(int input)
        {
            await Task.Delay(100);
            return;
        }
    }
}