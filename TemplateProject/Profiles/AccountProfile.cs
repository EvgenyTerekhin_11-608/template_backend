using AutoMapper;
using TemplateProject.Authentication;
using TemplateProject.Controllers;

namespace TemplateProject.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<SignupDto, User>();
        }
    }
}