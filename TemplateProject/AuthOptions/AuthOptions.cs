using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace TemplateProject.AuthOptions
{
    public static class AuthOptions
    {
        public static string ValidIssuer = "Server";
        public static string ValidAudience = "localhost:5000";

        private static string Key = "HackathonJWTTokenKey";

        // время жизни токена -  2 дня
        public static int LifeTimeDays = 2;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}