using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using static TemplateProject.AuthOptions.AuthOptions;

namespace TemplateProject.ServiceCollectionExtensions
{
    public static class AuthenticationExtensions
    {
        public static IServiceCollection AuthenticateRules(this IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = ValidIssuer,
                        ValidateAudience = true,
                        ValidAudience = ValidAudience,
                        ValidateLifetime = true,
                        IssuerSigningKey = GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true
                    };
                });
            return services;
        }
    }
}