using System;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace TemplateProject.ServiceCollectionExtensions
{
    public class Info : OpenApiInfo
    {
        public string Title { get; set; }
        public string Version { get; set; }
    }

    public static class AddSwaggerExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection sc)
        {
            sc.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info {Title = "My API", Version = "v1"});
            });
            return sc;
        }
    }
}