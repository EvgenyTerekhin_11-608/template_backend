using Microsoft.EntityFrameworkCore;

namespace TemplateProject.Authentication
{
    public class UsersDbContext : DbContext
    {
        public UsersDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}