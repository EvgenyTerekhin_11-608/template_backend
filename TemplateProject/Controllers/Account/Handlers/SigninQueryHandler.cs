using System.Threading.Tasks;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    public class SigninQueryHandler : IAsyncQuery<SigninDto, EncodeJwtDto>
    {
        private IAsyncQuery<BaseAuthDto, EncodeJwtDto> _getEncodeDto { get; set; }

        public SigninQueryHandler(IAsyncQueryHandler<BaseAuthDto, EncodeJwtDto> getEncodeDto)
        {
            _getEncodeDto = getEncodeDto;
        }

        public async Task<EncodeJwtDto> Query(SigninDto input) => await _getEncodeDto.Query(input);
    }
}