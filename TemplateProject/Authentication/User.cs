using System.ComponentModel.DataAnnotations;

namespace TemplateProject.Authentication
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        
        public int Age { get; set; }
        
    }

    public class UserDto
    {
        [Required]
        public string Name { get; set; }

        [Required, EmailAddress]
        public string Surname { get; set; }
        
    }
}