using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    public class SignupHandler : IAsyncQueryHandler<BaseAuthDto, EncodeJwtDto>
    {
        private readonly IAsyncQuery<BaseAuthDto, ClaimsIdentity> _getClaimsQuery;

        public SignupHandler(IAsyncQuery<BaseAuthDto, ClaimsIdentity> getClaimsQuery)
        {
            _getClaimsQuery = getClaimsQuery;
        }

        public async Task<EncodeJwtDto> Query(BaseAuthDto input)
        {
            var identity = await _getClaimsQuery.Query(input);
            if (identity == null)
                return null;
            var now = DateTime.UtcNow;
            var claims = identity.Claims;
            var jwt = new JwtSecurityToken(
                AuthOptions.AuthOptions.ValidIssuer,
                AuthOptions.AuthOptions.ValidAudience,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromDays(AuthOptions.AuthOptions.LifeTimeDays)),
                signingCredentials: new SigningCredentials(AuthOptions.AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var resultWrapper = new EncodeJwtDto
            {
                Email = claims.First(x => x.Type == "email").Value,
                Name = identity.Name,
                Surname = claims.First(x => x.Type == "surname").Value,
                EncodedJWT = encodedJwt
            };

            return resultWrapper;
        }
    }
}