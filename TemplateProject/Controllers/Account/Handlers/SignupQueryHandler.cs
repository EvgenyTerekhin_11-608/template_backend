using System.Threading.Tasks;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    public class SignupQueryHandler : IAsyncQuery<SignupDto, EncodeJwtDto>
    {
        private readonly IAsyncHandler<BaseAuthDto> _signupHandler;
        private readonly IAsyncQueryHandler<SignupDto, EncodeJwtDto> _getEncodeDto;

        public SignupQueryHandler(IAsyncHandler<BaseAuthDto> signupHandler,
            IAsyncQueryHandler<SignupDto, EncodeJwtDto> getEncodeDto)
        {
            _signupHandler = signupHandler;
            _getEncodeDto = getEncodeDto;
        }

        public async Task<EncodeJwtDto> Query(SignupDto input)
        {
            await _signupHandler.Handle(input);
            var result = await _getEncodeDto.Query(input);
            return result;
        }
    }
}