using System.ComponentModel.DataAnnotations;

namespace TemplateProject.Controllers
{
    public class SignupDto : BaseAuthDto
    {
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        [MaxLength(60)]
        public string Surname { get; set; }

        [Required]
        [Range(6, 100)]
        public int Age { get; set; }
    }
}