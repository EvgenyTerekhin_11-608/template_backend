using System;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace TemplateProject.ServiceCollectionExtensions
{
    public static class AddAutoMapperRegistrationsExtensions
    {
        public static IServiceCollection AddProfilesAutoRegistration(this IServiceCollection serviceCollection)
        {
            var profiles = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => x.BaseType == typeof(Profile));

            serviceCollection.AddAutoMapper(x => x.AddProfiles(profiles));
            return serviceCollection;
        }
    }
}