using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TemplateProject.Authentication;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    public class GetIdentityInfo : IAsyncQuery<BaseAuthDto, ClaimsIdentity>
    {
        private readonly UsersDbContext _context;

        public GetIdentityInfo(UsersDbContext context)
        {
            _context = context;
        }

        public async Task<ClaimsIdentity> Query(BaseAuthDto input)
        {
            var user = await _context.Users.Where(x => string.Equals(x.Email, input.Email)).FirstOrDefaultAsync();
            if (user == null)
                return null;
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim("id", user.Id.ToString()),
                new Claim("email", user.Email),
                new Claim("name", user.Name),
                new Claim("surname", user.Surname)
            };
            var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}