using Microsoft.AspNetCore.Mvc;
using TemplateProject.Authentication;

namespace TemplateProject.Controllers
{
    [Route("/api/[controller]/[action]")]
    public class ApiControllerBase : ControllerBase
    {
        protected readonly UsersDbContext _context;

        public ApiControllerBase(UsersDbContext context)
        {
            _context = context;
        }

        protected IActionResult OkOrBad<T>(T obj, string errorMessage = "badRequest") where T : class =>
            obj == null ?(ObjectResult) BadRequest(errorMessage) : Ok(obj);
    }
}