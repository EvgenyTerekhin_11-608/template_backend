namespace TemplateProject.Controllers
{
    public class EncodeJwtDto
    {
        public string EncodedJWT { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
    }
}