﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get(
            [FromServices] IQuery<int, int> query,
            [FromServices] IAsyncQuery<int, int> asyncQuery,
            [FromServices] IHandler<int> handler,
            [FromServices] IAsyncHandler<int> asyncHandler)
        {
            query.Query(1);
            await asyncQuery.Query(1);
            handler.Handle(1);
            await asyncHandler.Handle(1);
            return new[] {"value1", "value2"};
        }

        [HttpGet("api/authorize/test")]
        [Authorize]
        // GET api/values/5
        public ActionResult<string> Get()
        {
            return "ok";
        }

        // POST api/values
        [HttpGet("get")]
        public void Post()
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}