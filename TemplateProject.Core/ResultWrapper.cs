using Microsoft.AspNetCore.Mvc;

namespace TemplateProject.Controllers
{
    public abstract class ResultWrapper<TResult> where TResult : class
    {
        public static SuccessWrapper<TResult> Success(TResult data) => new SuccessWrapper<TResult>(data);

        public static FailedWrapper<TResult> Failed(string message) => new FailedWrapper<TResult>(message);

        protected abstract bool IsSuccess { get; }

    }

    public class FailedWrapper<TResult> : ResultWrapper<TResult> where TResult : class
    {
        public FailedWrapper(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        protected override bool IsSuccess { get; } = false;

        public string ErrorMessage { get; set; }
    }

    public class SuccessWrapper<TResult> : ResultWrapper<TResult> where TResult : class
    {
        public SuccessWrapper(TResult value)
        {
            _data = value;
        }

        protected TResult _data { get; set; }

        public TResult Data => _data;
        protected override bool IsSuccess { get; } = true;
    }
}