using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace TemplateProject.Query
{
    public interface IQuery<in TIn, out TOut>
    {
        TOut Query(TIn input);
    }

    public interface IAsyncQuery<in TIn, TOut>
    {
        Task<TOut> Query(TIn input);
    }

    public interface IAsyncQueryHandler<TIn, TOut> : IAsyncQuery<TIn, TOut>
    {
    }

    public interface IQueryHandler<in TIn> : IHandler<TIn>
    {
    }

    public interface IHandler<in TIn>
    {
        void Handle(TIn input);
    }

    public interface IAsyncHandler<TIn>
    {
        Task Handle(TIn input);
    }
}