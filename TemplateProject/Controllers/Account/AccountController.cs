using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TemplateProject.Authentication;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    [ApiController]
    public class AccountController : ApiControllerBase
    {
        private readonly UsersDbContext _context;

        public AccountController(UsersDbContext context) : base(context)
        {
            _context = context;
        }

        /// <summary>
        /// Метод для регистрации пользователя
        /// </summary>
        /// <param name="dto">информация о пользователе</param>
        /// <returns>JWT Token и информация о пользователе</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Signup(
            [FromBody] SignupDto dto,
            [FromServices] IAsyncQuery<SignupDto, EncodeJwtDto> signupQueryHandler) =>
            OkOrBad(await signupQueryHandler.Query(dto), "Данные не корректны");

        /// <summary>
        /// Метод для  авторизации пользователя
        /// </summary>
        /// <param name="dto">информация о пользователе</param>
        /// <returns>JWT Token и информация о пользователе</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Signin(
            [FromBody] SigninDto dto,
            [FromServices] IAsyncQuery<SigninDto, EncodeJwtDto> signinHandler) =>
            OkOrBad(await signinHandler.Query(dto), "Данные не корректны");
    }
}