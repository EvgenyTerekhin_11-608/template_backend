using System.Threading.Tasks;
using AutoMapper;
using TemplateProject.Authentication;
using TemplateProject.Query;

namespace TemplateProject.Controllers
{
    public class AddUserHandler : IAsyncHandler<SignupDto>
    {
        private readonly UsersDbContext _context;
        private readonly IMapper _mapper;

        public AddUserHandler(UsersDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task Handle(SignupDto input)
        {
            var user = _mapper.Map<User>(input);
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
        }
    }
}