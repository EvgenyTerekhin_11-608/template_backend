using System.ComponentModel.DataAnnotations;

namespace TemplateProject.Controllers
{
    public class BaseAuthDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [Compare(nameof(Password), ErrorMessage = "password != confirmpassword")]
        public string ConfirmPassword { get; set; }
    }
}